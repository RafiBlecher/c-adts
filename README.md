# C ADTs

A collection of Abstract Data Types written in C, for practice and to practice
other programming questions.

## Linked Lists

## Trees
* Binary Trees
    * Binary Search Trees
* Red-Black Trees
* 2-3-4 Trees
* Prefix Trees
* Suffix Trees

## Lookup Tables

## Hash Tables
* Hash Collision Algorithms
    * Linear probing
    * Double-hashing
    * Chaining

## Graphs
* Weighted graphs
* Unweighted graphs
* Algorithms
    * Searching
        * BFS
        * DFS
    * Path-finding
        * Prim's
        * Kruskal's
        * Hamilton Path/Tour
        * Euler Path/Tour 
        * Djikstra's
        * A*
