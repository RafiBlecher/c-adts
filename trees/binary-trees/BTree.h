// BTree.h - an interface for a binary tree ADT
// Written by Rafi Blecher, March 2014

// Stop this from being #included multiple times
#ifndef BTREE_H
#define BTREE_H

#include "Item.h"

typedef struct BTreeRep *BTree;
typedef struct BTNode *Link;

// create a new BTree
BTree newBTree();
// free a BTree
void dropBTree(BTree);
// Pretty-print a BTree
void printBTree(BTree);

// print values in infix order
void BTreeInfix(BTree);
void BTreeInfixFromLink(Link);
// print values in prefix order
void BTreePrefix(BTree);
void BTreePrefixFromLink(Link);
// print values is postfix order
void BTreePostfix(BTree);
void BTreePostfixFromLink(Link);

// count number of nodes in BTree
int BTreeNumLinks(BTree);
// count number of leaves in BTree
int BTreeNumLeaves(BTree);

// insert a new value into a BTree
void BTreeInsert(BTree, Item);
// insert a new value into a Binary Search Tree (BSTree)
void BSTreeInsert(BTree, Item);
// check whether a value is in a BTree
int BTreeFind(BTree, Item);
// check whether a value is in a BSTree
int BSTreeFind(BTree, Item);

#endif
