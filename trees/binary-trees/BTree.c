// BTree.c - implements a binary tree ADT
// Written by Rafi Blecher, March 2014

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "BTree.h"

// macros
#define leftChild(x) ((x)->children[0])
#define rightChild(x) ((x)->children[1])

// #defs
#define BTREE_NUM_CHILDREN 2
#define TRUE 1
#define FALSE 0

typedef struct BTNode {
    Item value;
    Link *children;
} BTNode;

typedef struct BTreeRep {
    Link root;
    int numNodes;
} BTreeRep;

// Prototypes
static Link newBTNode(Item);
static void drop(Link);
static void postOrderPrint(Link, int);
static int countLeaves(Link);
static void doBSTreeInsert(Link, Item, int *);
static int doBSTreeFind(Link, Item);

// Helper: make a new node containing a value
static Link newBTNode(Item v) {
    Link new = malloc(sizeof(BTNode));
    assert(new != NULL);

    new->value = v;
    // Children is an array of 2 nodes
    // children[0] is the left child
    // children[1] is the right child
    new->children = malloc(BTREE_NUM_CHILDREN * sizeof(BTNode));
    int i;
    for (i = 0; i < BTREE_NUM_CHILDREN; i++)
        new->children[i] = NULL;

    return new;
}

// Interface: create a new BTree
BTree newBTree() {
    BTree new = malloc(sizeof(BTreeRep));
    assert(new != NULL);

    new->root = NULL;
    new->numNodes = 0;

    return new;
}

// Interface: free a BTree
void dropBTree(BTree t) {
    if (t == NULL) return;

    drop(t->root);

    free(t);
    t = NULL;
}

// Helper: recursive drop tree
static void drop(Link l) {
    if (l == NULL) return;


    int i;
    for (i = 0; i < BTREE_NUM_CHILDREN; i++)
        drop(l->children[i]);

    free(l);
    l = NULL;
}

// Interface: pretty-print a BTree
void printBTree(BTree t) {
    postOrderPrint(t->root, 0);
}

// Helper: recursive pretty-print a tree
static void postOrderPrint(Link l, int indent) {
    if (l != NULL) {
        if ( leftChild(l) != NULL ) postOrderPrint( leftChild(l), indent+4 );
        if ( rightChild(l) != NULL ) postOrderPrint( rightChild(l), indent+4 );
        int i;
        for (i = 0; i < indent; i++)
            printf(" ");

        printItem(l->value); printf("\n ");
    }
}

// Interface: print values in infix order
void BTreeInfix(BTree t) {
    BTreeInfixFromLink(t->root);
}
void BTreeInfixFromLink(Link l) {
    if (l == NULL) return;
    BTreeInfixFromLink( leftChild(l) );
    printItem(l->value); printf(" ");
    BTreeInfixFromLink( rightChild(l) );
}

// Interface: print values in prefix order
void BTreePrefix(BTree t) {
    BTreePrefixFromLink(t->root);
}
void BTreePrefixFromLink(Link l) {
    if (l == NULL) return;
    printItem(l->value); printf(" ");
    BTreeInfixFromLink( leftChild(l) );
    BTreeInfixFromLink( rightChild(l) );
}

// Interface: print values in postfix order
void BTreePostfix(BTree t) {
    BTreePostfixFromLink(t->root);
}
void BTreePostfixFromLink(Link l) {
    if (l == NULL) return;
    BTreeInfixFromLink( leftChild(l) );
    BTreeInfixFromLink( rightChild(l) );
    printItem(l->value); printf(" ");
}

// Interface: count number of nodes in BTree
int BTreeNumNodes(BTree t) {
    return t->numNodes;
}

// Interface: count number of leaves in BTree
int BTreeNumLeaves(BTree t) {
    if (t == NULL) return 0;

    return countLeaves(t->root);
}

// Helper: count leaves in tree
int countLeaves(Link l) {
    if ( l == NULL) return 0;
    if ( leftChild(l) == NULL && rightChild(l) == NULL ) return 1;

    return countLeaves( leftChild(l) ) + countLeaves( rightChild(l) );
}

// Interface: insert a new value into a BTree
void BTreeInsert(BTree t, Item v) {
    printf("TODO... is this a balanced insertion, or what?");
}

// Interface: insert a new value into a BSTree
void BSTreeInsert(BTree t, Item v) {
    int problem = FALSE;
    doBSTreeInsert(t->root, v, &problem);

    if (!problem)
        t->numNodes++;
}

// Helper: recursive insert binary search tree
static void doBSTreeInsert(Link l, Item v, int *problem) {
    // if v is equal to the value of the current node, don't insert
    if ItemEQ(v, l->value) {
        *problem = TRUE;
        return;
    }

    // Change the value of the pointer rather than
    // returning and changing the pointer
    if (l == NULL) *l = *newBTNode(v);

    // ItemGT(a, b) returns 1 if b < a, else 0.
    // If v is "less than" l->value, we want to insert left.
    // Left child is l->children[0].
    // So, we want to evaluate ItemGT(v, l->value). If l->value > v, v is
    // "less than" the value of the current node, and thus we want to insert
    // to the left AND ItemGT(v, l->value) returns 0 to give us l->children[0],
    // heading left.
    // Obviously the behaviour is reversed when v > l->value, and we get 1 to
    // head right instead.
    doBSTreeInsert(l->children[ ItemGT(v, l->value) ], v, problem);
}

// Interface: check whether a value is in a BTree
int BTreeFind(BTree t, Item v) {
    printf("TODO... basically just an infix search, right?");
    return 1;
}

// Interface: check whether a value is in a BSTree
int BSTreeFind(BTree t, Item v) {
    if (t == NULL) return FALSE;

    return doBSTreeFind(t->root, v);
}

static int doBSTreeFind(Link l, Item v) {
    if (l == NULL) return FALSE;
    if ( ItemEQ(l->value, v) ) return TRUE;

    return doBSTreeFind(l->children[ ItemGT(v, l->value) ], v);
}
